﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	public class CsvSerializer : ISerializer
	{
		public string Serialize<T>(T value)
		{
			var props = value.GetType().GetProperties();
			StringBuilder result = new StringBuilder();
			foreach (var a in props)
			{
				result.Append(a.GetValue(value).ToString());
				result.Append(',');
			}
			result.Remove(result.Length - 1, 1);
			return result.ToString();
		}
	}
}
