﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	public class Settings
	{
		private static Settings _instance;

		public string Path { get; set; }

		protected Settings()
		{

		}

		public static Settings getSettings()
		{
			if (_instance == null)
			{
				_instance = new Settings();
			}
			return _instance;
		}
	}
}
