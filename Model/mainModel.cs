﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	public class mainModel : IModel
	{
		private ISerializer _serializer;
		private IWriter _writer;
		public mainModel(ISerializer serializator, IWriter fileworker)
		{
			_serializer = serializator;
			_writer = fileworker;
		}
		public void Save(User toSave)
		{
			_writer.Write(Settings.getSettings().Path, _serializer.Serialize<User>(toSave));
		}
	}
}
