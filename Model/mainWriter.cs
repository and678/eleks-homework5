﻿using System;
using System.IO;

namespace Model
{
	public class mainWriter : IWriter
	{
		public void Write(string where,string toWrite)
		{
			Directory.CreateDirectory(Path.GetDirectoryName(where));
			using (StreamWriter writer = new StreamWriter(where, true))
			{
				writer.WriteLine(toWrite);
			}
		}
	}
}
