﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
	[TestFixture]
	public class CsvSerializerTests
	{
		[Test]
		public void Serialize_User_Success()
		{
			CsvSerializer tested = new CsvSerializer();

			User state = new User()
			{
				Name = "aAa",
				SurName = "bBb",
				Age = 5,
				Work = "cCc"
			};
			string result = tested.Serialize(state);
			Assert.That(result, Is.EqualTo("aAa,bBb,5,cCc"));
		}
	}
}
