﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
	public partial class mainForm : Form, IView
	{
		public mainForm()
		{
			InitializeComponent();
		}

		public string Age
		{
			get {
				return ageTextBox.Text;
			}
		}

		public string FirstName
		{
			get {
				return fnameTextBox.Text;
			}
		}

		public string Occupation
		{
			get {
				return occupationTextBox.Text;
			}
		}

		public string SurName
		{
			get {
				return lnameTextBox.Text;
			}
		}

		public string Path
		{
			get {
				return fileTextBox.Text;
			}
		}

		public event Action NewUser;
		public event Action NewPath;

		public void AddHistoryRecord(string record)
		{
			historyTextBox.Text = historyTextBox.Text + record + Environment.NewLine;
		}

		private void browseButton_Click(object sender, EventArgs e)
		{
			DialogResult result = fileDialog.ShowDialog();
			if (result == DialogResult.OK)
			{
				fileTextBox.Text = fileDialog.FileName;
			}
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			NewUser();  //fire event
		}
		private void setButton_Click(object sender, EventArgs e)
		{
			NewPath();  //fire event
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
