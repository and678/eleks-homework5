﻿namespace View
{
	partial class mainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fileDialog = new System.Windows.Forms.OpenFileDialog();
			this.addButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.browseButton = new System.Windows.Forms.Button();
			this.fileTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fnameTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.lnameTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.ageTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.occupationTextBox = new System.Windows.Forms.TextBox();
			this.historyTextBox = new System.Windows.Forms.TextBox();
			this.setButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// fileDialog
			// 
			this.fileDialog.FileName = "openFileDialog1";
			// 
			// addButton
			// 
			this.addButton.Location = new System.Drawing.Point(12, 312);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(75, 23);
			this.addButton.TabIndex = 0;
			this.addButton.Text = "Add record";
			this.addButton.UseVisualStyleBackColor = true;
			this.addButton.Click += new System.EventHandler(this.addButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.Location = new System.Drawing.Point(363, 312);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(75, 23);
			this.exitButton.TabIndex = 0;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// browseButton
			// 
			this.browseButton.Location = new System.Drawing.Point(282, 31);
			this.browseButton.Name = "browseButton";
			this.browseButton.Size = new System.Drawing.Size(75, 23);
			this.browseButton.TabIndex = 0;
			this.browseButton.Text = "Browse";
			this.browseButton.UseVisualStyleBackColor = true;
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// fileTextBox
			// 
			this.fileTextBox.Location = new System.Drawing.Point(13, 34);
			this.fileTextBox.Name = "fileTextBox";
			this.fileTextBox.Size = new System.Drawing.Size(263, 20);
			this.fileTextBox.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(105, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Choose file location: ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(10, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "First name: ";
			// 
			// fnameTextBox
			// 
			this.fnameTextBox.Location = new System.Drawing.Point(135, 60);
			this.fnameTextBox.Name = "fnameTextBox";
			this.fnameTextBox.Size = new System.Drawing.Size(303, 20);
			this.fnameTextBox.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Last name: ";
			// 
			// lnameTextBox
			// 
			this.lnameTextBox.Location = new System.Drawing.Point(135, 86);
			this.lnameTextBox.Name = "lnameTextBox";
			this.lnameTextBox.Size = new System.Drawing.Size(303, 20);
			this.lnameTextBox.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(10, 116);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(29, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Age:";
			// 
			// ageTextBox
			// 
			this.ageTextBox.Location = new System.Drawing.Point(135, 112);
			this.ageTextBox.Name = "ageTextBox";
			this.ageTextBox.Size = new System.Drawing.Size(303, 20);
			this.ageTextBox.TabIndex = 4;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(10, 142);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(68, 13);
			this.label5.TabIndex = 3;
			this.label5.Text = "Occupation: ";
			// 
			// occupationTextBox
			// 
			this.occupationTextBox.Location = new System.Drawing.Point(135, 138);
			this.occupationTextBox.Name = "occupationTextBox";
			this.occupationTextBox.Size = new System.Drawing.Size(303, 20);
			this.occupationTextBox.TabIndex = 4;
			// 
			// historyTextBox
			// 
			this.historyTextBox.Location = new System.Drawing.Point(12, 165);
			this.historyTextBox.Multiline = true;
			this.historyTextBox.Name = "historyTextBox";
			this.historyTextBox.ReadOnly = true;
			this.historyTextBox.Size = new System.Drawing.Size(426, 141);
			this.historyTextBox.TabIndex = 5;
			// 
			// setButton
			// 
			this.setButton.Location = new System.Drawing.Point(363, 31);
			this.setButton.Name = "setButton";
			this.setButton.Size = new System.Drawing.Size(75, 23);
			this.setButton.TabIndex = 0;
			this.setButton.Text = "Set";
			this.setButton.UseVisualStyleBackColor = true;
			this.setButton.Click += new System.EventHandler(this.setButton_Click);
			// 
			// mainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(450, 344);
			this.Controls.Add(this.historyTextBox);
			this.Controls.Add(this.occupationTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.ageTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.lnameTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.fnameTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.fileTextBox);
			this.Controls.Add(this.setButton);
			this.Controls.Add(this.browseButton);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.addButton);
			this.Name = "mainForm";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog fileDialog;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.TextBox fileTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox fnameTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox lnameTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox ageTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox occupationTextBox;
		private System.Windows.Forms.TextBox historyTextBox;
		private System.Windows.Forms.Button setButton;
	}
}

