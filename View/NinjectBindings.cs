﻿using System;
using Ninject;
using Ninject.Modules;
using View;
using Model;

namespace View
{
	class NinjectBindings : NinjectModule
	{
		public override void Load()
		{
			Bind<IView>().To<mainForm>();
			Bind<IModel>().To<mainModel>();
			Bind<IWriter>().To<mainWriter>();
			Bind<ISerializer>().To<CsvSerializer>();
		}
	}
}
