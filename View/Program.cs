using System;
using System.Windows.Forms;
using Ninject;
using Presenter;
using Model;

namespace View
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			IKernel kernel = new StandardKernel(new NinjectBindings());
			mainPresenter presenter = kernel.Get<mainPresenter>();
			presenter.StartView();
		}
	}
}