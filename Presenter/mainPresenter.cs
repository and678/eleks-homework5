﻿using View;
using Model;
using System.Windows.Forms;
using System.Text;
using System;

namespace Presenter
{
    public class mainPresenter
    {
		private IView _view;
		private IModel _writer;

		public mainPresenter(IView v, IModel w)
		{
			_view = v;
			_writer = w;

			v.NewUser += RegisterUser;
			v.NewPath += RegisterPath;
		}

		private void RegisterPath()
		{
			StringBuilder message = new StringBuilder(DateTime.Now.ToString("h:mm:ss tt "));
			Settings sets = Settings.getSettings();
			sets.Path = _view.Path;
			message.Append("Sucessfully set a file location.");
			_view.AddHistoryRecord(message.ToString());
		}

		private void RegisterUser()
		{
			StringBuilder message = new StringBuilder(DateTime.Now.ToString("h:mm:ss tt "));

			int newAge;
			bool ageTest = int.TryParse(_view.Age, out newAge);
			User current = new User()
			{
				Name = _view.FirstName,
				SurName = _view.SurName,
				Age = newAge,
				Work = _view.Occupation
			};
			if (String.IsNullOrEmpty(Settings.getSettings().Path))
			{
				message.Append("Error. Please specify a file.");
			}
			else if (String.IsNullOrEmpty(current.Name))
			{
				message.Append("Error. Please enter a valid name.");
			}
			else if (!ageTest)
			{
				message.Append("Error. Please enter a valid date.");
			}
			else if (String.IsNullOrEmpty(current.SurName))
			{
				message.Append("Error. Please enter a valid surname.");
			}
			else if (String.IsNullOrEmpty(current.Work))
			{
				message.Append("Error. Please enter a valid workplace.");
			}
			else
			{
				try
				{
					_writer.Save(current);
					message.Append(String.Format("Sucessfull added a user {0}.", current.Name));
				}
				catch (Exception e)
				{
					message.Append("Error. ");
					message.Append(e.Message);
				}
			}
			_view.AddHistoryRecord(message.ToString());
		}
		public void StartView()
		{
			Application.Run(_view as Form);
		}
    }
}
