﻿using System;

namespace View
{
	public interface IView
	{
		event Action NewUser;
		event Action NewPath;

		string FirstName { get; }
		string SurName { get; }
		string Age { get; }
		string Occupation { get; }

		string Path { get; }

		void AddHistoryRecord(string record);
	}
}
